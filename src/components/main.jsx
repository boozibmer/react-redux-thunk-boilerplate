import React, { Component } from 'react';
import { connect } from 'react-redux'
import { testReduxThunk } from 'actions/app'
import Sample from './sample.jsx';

class Main extends Component {
    componentDidMount() {
        this.props.dispatch(testReduxThunk());
    }
    render() {
        const { title, description } = this.props.app;
        return (
            <Sample title={title} description={description} />
        );
    }
}

const mapstateToProps = ({ app }) => {
    return { app };
};

export default connect(mapstateToProps)(Main);