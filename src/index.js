import 'typeface-roboto';
import React from "react";
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Main from './components/main.jsx';
import reducers from './reducers';

const store = createStore(
    reducers,
    applyMiddleware(thunk)
);

const container = document.getElementById("container");
container ? ReactDOM.render(<Provider store={store}><Main /></Provider>, container) : false;