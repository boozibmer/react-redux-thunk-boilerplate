const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
  module: {
    rules: [
        {
          resolve: {
            alias: {
              actions: path.resolve(__dirname, 'src/actions/'),
              constants: path.resolve(__dirname, 'src/constants/'),
              reducers: path.resolve(__dirname, 'src/reducers/'),
              reducers: path.resolve(__dirname, 'src/components/'),
            }
          },
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
              loader: "babel-loader"
          }
        },
        {
          test: /\.html$/,
          use: [{
              loader: "html-loader"
          }]
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.(woff|woff2|eot|ttf|svg)$/,
          use: [{
              loader: 'file-loader'
          }]
        },
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ]
};